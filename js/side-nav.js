;(function($){

    // Hard code the site nav within JS file so we can run page locally without a server
    var sideNav = [
        'AMC',
        'Belersdorf',
        {
            name : 'DST',
            selections : [
                'Option 1'
            ]
        },
        {
            name : 'Electrolux',
            selections : [
                'Option 1'
            ]
        },
        'Fox',
        'Galderma',
        'Investa',
        'MCN',
        {
            name : 'Merisant',
            selections : [
                {
                    name : 'Equal - APAC',
                    selections : [
                        'Design',
                        'Stage'
                    ]
                }
            ]
        },
        {
            name : 'Orchard',
            selections : [
                'Option 1'
            ]
        },
        {
            name : 'Pfizer',
            selections : [
                'Option 1'
            ]
        },
        {
            name : 'Variety',
            selections : [
                'Option 1'
            ]
        },
        'Video Ezy'
    ];

    var generateURL = function(str){
        return str.replace(' ', '+')
    };

    var generateMenuItem = function(title, crumbTrail, hasChildren){

        var $listItem,
            htmlMarkup = '<a href="#/'+ generateURL( crumbTrail.join('/') ) +'">'+ title +'</a>';

        if(hasChildren){
            htmlMarkup = '<span class="side-nav__toggle side-nav__toggle--open pull-left"></span>' + htmlMarkup;
        }else{
            htmlMarkup = '<span class="side-nav__toggle pull-left"></span>' + htmlMarkup;
        }

        $listItem =  $('<li />').html(htmlMarkup);
        $listItem.find('a').data('breadcrumbs', crumbTrail.slice());

        return $listItem;
    };

    var crumbTrail = [];

    var generateMenu = function(options, $ul){
        var i = 0, title, $listItem = null;

        for(i; i < options.length; i++){

            if(typeof options[i] === 'string'){
                crumbTrail.push(options[i]);
                $listItem = generateMenuItem(options[i], crumbTrail);
            }else{
                title = options[i].name;
                crumbTrail.push(title);

                if( typeof options[i].selections !== 'undefined'){
                    var $listItemUl = $('<ul />');

                    $listItem = generateMenuItem(title, crumbTrail, true);
                    $listItemUl = generateMenu(options[i].selections, $listItemUl);
                    $listItemUl.hide();
                    $listItem.append($listItemUl);
                }else{
                    $listItem = generateMenuItem(title, crumbTrail);
                }
            }

            crumbTrail.pop();
            $ul.append($listItem);
        }

        return $ul
    };

    $(window).ready(function(){
        var $siteNavList = $('<ul />').addClass('side-nav__list');

        $siteNavList = generateMenu(sideNav, $siteNavList);

        $('.side-nav').html($siteNavList);

        $('.side-nav__toggle').click(function(e){
            var $this = $(this);

            if(!$this.hasClass('side-nav__toggle--open') && !$this.hasClass('side-nav__toggle--close'))
                return;

            $this.toggleClass('side-nav__toggle--open side-nav__toggle--close');
            $this.siblings('ul').stop().slideToggle(function(){
                // Bug when clicking quickly, keeps height style
                // and internal embeds are positioned over font
                $(this).css('height', '');
            });

            if($this.hasClass('side-nav__toggle--close')){
                $this.next().css('font-weight', 'bold');
            }else{
                $this.next().css('font-weight', 'normal');
            }

            e.stopPropagation();
        });

        $('.side-nav a').click(function(e){
            var htmlMarkup = [], i = 0,
                $that = $(this),
                $breadCrumb = $('#breadcrumb'),
                trail = $that.data('breadcrumbs');

            for(i; i < trail.length; i++){

                var link = trail.slice(0, i + 1);

                if(i !== 0)
                    htmlMarkup.push(' > ');

                htmlMarkup.push('<a href="#/' + generateURL( link.join('/') ) + '">' + trail[i] + '</a>');
            }

            $breadCrumb.html(htmlMarkup.join(''));

        });

    });

})(jQuery);