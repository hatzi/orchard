angular.module('App', [])
    .controller('PageController', ['$scope', function($scope){

        $scope.isEditMode = false;

        $scope.versionControl = {
            repository  : 'https://bitbucket.org/hatzi/orchard.git',
            viewReop    : 'https://bitbucket.org/hatzi/orchard.git',
            activityLog : 'https://bitbucket.org/hatzi/orchard/commits/all'
        };

        $scope.websites = [
            {
                title: 'Description:',
                url: 'localhost',
                confirm: true
            },
            {
                title: 'Url:',
                url: 'http://localhost/',
                confirm: true
            }
        ];

        $scope.setEditMode = function(isEditMode){
            $scope.isEditMode = isEditMode;
        };

        $scope.addSite = function(){
            var site = {
                title: 'Heading:',
                url: 'http://mySite.com/'
            }
            $scope.websites.push(site);
        }

        $scope.confirmSite = function(index){
            $scope.websites[index].confirm = true;
        }

    }]);