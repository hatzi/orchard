describe('PageController', function(){
    beforeEach(module('App'));

    var $scope,
        $ctrl

    beforeEach(inject(function($rootScope, $controller){
        $scope = $rootScope.$new();
        $ctrl = $controller('PageController', {
            $scope: $scope
        });
    }));

    it('should have set some version control properties', function(){
        expect($scope.versionControl.repository).toBeDefined();
        expect($scope.versionControl.viewReop).toBeDefined();
        expect($scope.versionControl.activityLog).toBeDefined();
    });

    it('should have set 2 websites', function(){
        expect($scope.websites.length).toEqual(2);
    });

    it('should set the edit mode to true', function(){
        expect($scope.isEditMode).toBeFalsy();
        $scope.setEditMode(true);
        expect($scope.isEditMode).toBeTruthy();
    });

    it('should add a website to the website List', function(){
        $scope.addSite();
        expect($scope.websites.length).toEqual(3);
    });

    it('should set the new added website "confirm" to true once changes made', function(){
        $scope.addSite();
        $scope.websites[2].title = "Theo";
        expect($scope.websites[2].confirm).toBeUndefined();
        $scope.confirmSite(2);
        expect($scope.websites[2].confirm).toBeTruthy();
    });

});