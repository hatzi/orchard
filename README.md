# README #

This repo is a simple code test which involved recreating an HTML page with the side navigation created using JS/JQuery. I have also incorporated a bit of angular with the Add/Edit functions just for kicks :)

### How do I run this project? ###

* Ensure you have Node JS and Gulp installed (make sure gulp is installed globally)
* Clone this repo to your machine
* Open up your console, and `cd` into the project folder and run `npm install`
* Run `gulp build`, or just `gulp` to skip the next step
* You can now open `app/index.html` in your browser