var gulp = require('gulp');
var browserSync = require('browser-sync');

// Reload all Browsers
gulp.task('bs-reload', function () {
    browserSync.reload();
});

gulp.task('watch', function(){
    gulp.start('serve');

    gulp.watch(['./js/**/*.js'], ['js']);
    gulp.watch(['./scss/**/*.scss'], ['css']);
});