'use strict';

var gulp = require('gulp'),
    sass = require('gulp-sass'),
    browserSync = require('browser-sync'),
    minifycss = require('gulp-minify-css'),
    gulpif = require('gulp-if'),
    args   = require('yargs').argv,
    gutil = require('gulp-util'),
    reload = browserSync.reload,
    isProduction = args.type === 'production',
    scssPaths = [
        './scss',
        './node_modules/bootstrap-sass/assets/stylesheets'

    ];

gulp.task('css', function () {
    return gulp.src('scss/main.scss')
        .pipe(sass({
            includePaths: scssPaths,
            errLogToConsole: false,
            onError: function(error){
                gutil.log(gutil.colors.red(error));
            }
        }))
        .pipe(gulpif(isProduction, minifycss()))
        .pipe(gulp.dest('app/build'))
        .pipe(reload({stream:true}));
});