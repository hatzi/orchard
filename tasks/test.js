var gulp = require('gulp'),
    karma = require('karma').server,
    path = require('path');

var karmaConfig = path.normalize(__dirname + '/../karma.conf.js');

gulp.task('test', function(){
    karma.start({
        configFile: karmaConfig,
        singleRun: true
    });
});

gulp.task('tdd', function(){
    karma.start({
        autoWatch: true,
        configFile: karmaConfig,
        singleRun: false,
        autoWatchBatchDelay: 1000,
        browserNoActivityTimeout: 5000
    });
});