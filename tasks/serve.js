'use strict';

var gulp = require('gulp');
var browserSync = require('browser-sync');

gulp.task('serve', ['build'], function() {

    browserSync({
        open: true,
        port: 9000,
        server: {
            baseDir: "./app",
        }
    });

});