'use strict';

var gulp = require('gulp'),
    uglify = require('gulp-uglify'),
    concat = require('gulp-concat'),
    gulpif = require('gulp-if'),
    args = require('yargs').argv,
    browserSync = require('browser-sync'),
    reload      = browserSync.reload,
    dest ='./app/build/';

var isProduction = args.type === 'production';

gulp.task('scripts', function(){
    return gulp.src(['./node_modules/jquery/dist/jquery.js', './node_modules/angular/angular.js', './js/**/*.js'])
        .pipe(concat('scripts.js'))
        .pipe(gulpif(isProduction, uglify()))
        .pipe(gulp.dest(dest + 'js/'));
});

gulp.task('main', ['scripts'], function(){

    return gulp.src(dest + 'js/scripts.js')
        .pipe(concat('main.js'))
        .pipe(gulpif(isProduction, uglify()))
        .pipe(gulp.dest(dest))
        .pipe(reload({stream:true}));
});

gulp.task('js', ['main']);